Kmonad
======

Install Kmonad, assuming ~/git for public repos:

```sh
git clone git@github.com:kmonad/kmonad.git
cd kmonad
docker build -t kmonad-builder .
docker run --rm -it -v ${PWD}:/host/ kmonad-builder bash -c 'cp -vp /root/.local/bin/kmonad /host/'
docker rmi kmonad-builder
```

This results in a `kmonad` binary.

Next, copy the systemd service file to `/etc/systemd/system`, make sure you 
have it reference the correct keyboard file, and enable it.

You can run `kmonad -d` to dry run (test your file) and with `--log-level info`
to get some more info in case of issues.
