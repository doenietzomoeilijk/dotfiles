Vim dotfiles
============

After `stow`ing vim, run the plugin installer:

`vim +PlugInstall`
