export ZSH=$HOME/.oh-my-zsh
export ZSH_CUSTOM=$HOME/.zsh-custom

ZSH_THEME="bureau"
CASE_SENSITIVE="false"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
HIST_STAMPS="yyyy-mm-dd"

zstyle :omz:plugins:ssh-agent quiet yes
# zstyle :omz:plugins:ssh-agent identities id_rsa id_ed25519

plugins=(
    direnv
    docker
    docker-compose
    git
    ssh-agent
    sudo
    zsh-autosuggestions
    zsh-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh

[ -f /usr/share/powerline/zsh/powerline.zsh ] && . /usr/share/powerline/zsh/powerline.zsh

export GOPATH=$HOME/go
export PATH=$HOME/.local/bin:$HOME/.local/share/JetBrains/Toolbox/bin:$GOPATH/bin:/opt/bin:$PATH
export XDG_DATA_DIRS=~/.local/share/applications:$XDG_DATA_DIRS

export SYMFONY_ENV=dev
export VISUAL=/usr/bin/vim
export EDITOR=$VISUAL

[ -d "$HOME/perl5/bin" ] && {
    export PATH="$HOME/perl5/bin${PATH:+:${PATH}}"
    export PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"
    export PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"
    export PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"
    export PERL_MB_OPT="--install_base \"$HOME/perl5\""
}

which thefuck > /dev/null
rc=$?; [[ $rc == 0 ]] && eval $(thefuck --alias)

autoload bashcompinit
bashcompinit
$(which register-python-argcomplete > /dev/null) && eval "$(register-python-argcomplete pmbootstrap)"

alias dcu="docker-compose up -d"
alias dcd="docker-compose down"
alias dcp="docker-compose pull"

# kdesrc-build #################################################################

## Add kdesrc-build to PATH
export PATH="$HOME/kde/src/kdesrc-build:$PATH"


## Autocomplete for kdesrc-run
function _comp_kdesrc_run
{
  local cur
  COMPREPLY=()
  cur="${COMP_WORDS[COMP_CWORD]}"

  # Complete only the first argument
  if [[ $COMP_CWORD != 1 ]]; then
    return 0
  fi

  # Retrieve build modules through kdesrc-run
  # If the exit status indicates failure, set the wordlist empty to avoid
  # unrelated messages.
  local modules
  if ! modules=$(kdesrc-run --list-installed);
  then
      modules=""
  fi

  # Return completions that match the current word
  COMPREPLY=( $(compgen -W "${modules}" -- "$cur") )

  return 0
}

## Register autocomplete function
complete -o nospace -F _comp_kdesrc_run kdesrc-run

################################################################################

autoload -U +X bashcompinit && bashcompinit

[ -f /usr/bin/terraform ] && complete -o nospace -C /usr/bin/terraform terraform

# Ignore ^D in tmux, based on the IGNOREEOF env variable being set.
if (( ${+IGNOREEOF} )); then
    setopt ignore_eof    
fi
