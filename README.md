# Glorious dotfiles!

Everything works with [Stow](https://www.gnu.org/software/stow/) now. Checkout
this repo to `~/dotfiles` (or whatever works, as long as it's directly under
`$HOME`), enter the directory, and use `stow <package>`, where `package` is one
of the subdirectories. So for example `stow zsh` properly symlinks everything up
for ZSH.

Some packages use git submodules, so you want to run a `git submodule update`
after checkout.

## Setting up ZSH and Oh My ZSH

_This requires `zsh` to be installed._

After setting up ZSH (`stow zsh`), you need to run the OMZ installer:

```sh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" \
    "" --keep-zshrc --unattended
```

This will walk you through changing your default shell. If this was already
done, you can add `--unattended` to the command.

## i3 Tiling WM

Requirements:

* i3-gaps
* polybar
* dunst
* picom
* alacritty
